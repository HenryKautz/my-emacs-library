(defun fixkindle ()
  (interactive)
  
  (goto-char (point-min))
  (while (re-search-forward "Read more at location" nil t)
    (let ((junkstart (match-beginning 0))
	  (junkend (match-end 0)))
      (goto-char junkend)
      (delete-region junkstart junkend)
      (insert " -- location ")
      (forward-line)
      (beginning-of-line)
      (setq junkstart (point))
      (re-search-forward "Note:")
      (setq junkend (match-beginning 0))
      (goto-char junkend)
      (delete-region junkstart junkend)
      (re-search-forward "Edit this note\\|Add a note")
      (let ((morejunkstart (match-beginning 0)))
	(end-of-line)
	(delete-region morejunkstart (point))
	(insert "\n")))))


	
	
    