;###################################################################
;#
;# Set up load path
;#
;###################################################################

(defun list-files (&rest files)
  "Return a list of those members of FILES which are readable.
If file does not exist, also check for .el and .elc postfixes.
Expand file names."
  (if (null files) nil
    (if (and (car files)
	     (or (file-readable-p (car files))
		 (file-readable-p (concat (car files) ".el"))
		 (file-readable-p (concat (car files) ".elc"))))
	(cons (expand-file-name (car files))
	      (apply 'list-files (cdr files)))
      (apply 'list-files (cdr files)))))

(setq load-path
      (append 
       (list-files "~/lib/emacs20" 
                   ) load-path))

;###################################################################
;#
;# key bindings
;#
;###################################################################


(defun add-to-hook-list (hook fnc)
  "augment HOOK list with function FNC"
  (if (not (boundp hook)) (set hook nil))
  (if (and (not (null (symbol-value hook))) 
	   (symbolp (symbol-value hook)))
      (set hook (list (symbol-value hook))))
  (set hook (append (symbol-value hook) (list fnc)))
  )

;;; Global Bindings

;; Killing & Copying

;		Kill		Copy
; line		C-k		C-w
; region	M-k		M-w
; sentence	M-K		M-W
; sexp		C-M-k		C-M-w
;
; Yank			C-y
; Yank-pop		M-y
; Append next kill 	C-M-y
;

(global-set-key "\eK" 'kill-sentence)
(global-set-key "\ek" 'kill-region)
(global-set-key "\eW" 'copy-sentence-as-kill)
(global-set-key "\ew" 'copy-region-as-kill)
(global-set-key "\C-w" 'copy-line-as-kill)
(global-set-key "\e\C-w" 'copy-sexp-as-kill)
(global-set-key "\e\C-y" 'append-next-kill)

;; Rearrangements

(global-set-key "\C-z" 'undo)
(global-set-key "\C-x\C-z" 'suspend-emacs)
(global-set-key "\C-o" 'other-window)
(global-set-key "\C-xS" 'save-buffer)
(global-set-key "\C-x\C-e" 'call-last-kbd-macro)
(global-set-key "\C-xe" 'eval-last-sexp)
(global-set-key "\C-x3" 'split-window-horizontally)
(global-set-key "\C-XS" 'shell)
(global-set-key "\C-x\C-j" 'goto-line)

(defun swap-local-find-tag-keys ()
  (interactive)
  "Move local binding if any from esc, and esc. to shifted keys,
and use the global meaning for the C-xo and C-o keys."
  (if (local-key-binding "\e,")
      (progn
	(local-set-key "\e<" (local-key-binding "\e,"))
	(local-unset-key "\e,")))
  (if (local-key-binding "\e.")
      (progn
	(local-set-key "\e>" (local-key-binding "\e."))
	(local-unset-key "\e.")))
  (if (local-key-binding "\C-xO")
      (local-unset-key "\C-xO"))
  (if (local-key-binding "\C-O")
      (local-unset-key "\C-O"))
  )

(let ((a (global-key-binding "\e<"))
      (b (global-key-binding "\e>")))
  (global-set-key "\e<" (global-key-binding "\e,"))
  (global-set-key "\e>" (global-key-binding "\e."))
  (global-set-key "\e," a)
  (global-set-key "\e." b)
  )

;; VT100 cursor keys

(global-set-key "\eOC" 'forward-char)
(global-set-key "\eOD" 'backward-char)
(global-set-key "\eOA" 'previous-line)
(global-set-key "\eOB" 'next-line)

;; New Commands

(global-set-key "\C-x=" 'what-cursor-and-line-position)
(global-set-key "\C-X\C-I" 'indent-to-cursor)
(global-set-key "\C-x2" 'split-window-vertically)
(global-set-key "\C-x1" 'delete-other-windows)
(global-set-key "\C-xo" 'swap-windows)
(global-set-key "\C-x\C-c" 'ask-before-quitting)

(add-to-hook-list 'dired-mode-hook 'swap-local-find-tag-keys)

;###################################################################
;#
;# Load Misc Functions
;#
;###################################################################

(require 'kautz-misc)

;###################################################################
;#
;# Buffer menu mode
;#
;###################################################################

(define-key Buffer-menu-mode-map "\C-o" 'other-window)

;###################################################################
;#
;# custom shell and compilation
;#
;###################################################################

(make-variable-buffer-local 'comint-completion-addsuffix)

(defun my-shell-setup ()
  "For bash (cygwin 18) under Emacs 20"
  (setq comint-scroll-show-maximum-output 'this)
  (setq comint-completion-addsuffix t)
  (setq comint-eol-on-send t)
  (setq w32-quote-process-args ?\")
  (setq comint-output-filter-functions '(ddd-track-directory t))
   (local-set-key "\C-c\C-s" 'shell-synchronize-directory)
   (local-set-key "\C-c\C-k" 'kill-all-output-from-shell)
   (local-set-key "\C-c\C-f" 'visit-file-pointer)
   ;; Add the working directory to the mode line
   (setq mode-line-buffer-identification '("%b  " default-directory))
   (setq mode-line-default-directory "")
   (swap-local-find-tag-keys)
)

(add-to-hook-list 'shell-mode-hook 'my-shell-setup)

(defun ddd-track-directory (text)
  (if (string-match "\\w*Working directory is ||\\([^|]+\\)||" text)
      (cd (substring text (match-beginning 1) (match-end 1)))))
    
(defun kill-all-output-from-shell ()
  "Kill shell buffer above current prompt."
  (interactive)
  (goto-char (point-max))
  (re-search-backward shell-prompt-pattern nil t)
  (kill-region (point-min) (point))
  (goto-char (point-max)))


;###################################################################
;#
;# terminal and shell specific settings
;#
;###################################################################

;; Always turn on metakeys
(set-variable 'meta-flag t)

;###################################################################
;#
;# suspension and quitting
;#
;###################################################################

;; Cautious suspension

; Suspend hook is not a proper hook list
(setq suspend-hook 'ask-before-suspending)

(defun ask-before-suspending ()
   "ask if user really wants to suspend"
   (interactive)
   (not (y-or-n-p "Really suspend Emacs? ")))

(defun ask-before-quitting ()
   (interactive)
   (if (y-or-n-p "Really quit? ")
      (save-buffers-kill-emacs)))

(defun quit ()
  (interactive)
  (save-buffers-kill-emacs 1)
  )



;###################################################################
;#
;# Colors and Frames
;#
;###################################################################

(setq number-of-shells 0)

(defun new-shell-other-frame ()
  (interactive)
  (select-frame (make-frame))
  (setq number-of-shells (+ 1 number-of-shells))
  (shell (concat "*shell-" (number-to-string number-of-shells) "*"))
  (delete-other-windows))

(setq shell-font-lock-keywords
'(("[ 	]\\([+-][^ 	\n]+\\)" 1 font-lock-comment-face)
 ("^[^ 	\n]+:.*" . font-lock-warning-face)
 ("^\\[[1-9][0-9]*\\]" . font-lock-string-face)))


(cond 
 (window-system
  (global-font-lock-mode 1)
  (global-set-key "\C-x\C-n" 'make-frame)
))

;###################################################################
;#
;# flags and autoloads
;#
;###################################################################

;; Standard Flags

(setq c-tab-always-indent t)
;(add-to-hook-list 'text-mode-hook 'turn-on-auto-fill)
;(add-to-hook-list 'text-mode-hook 'use-raw-tab)
(put 'eval-expression 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(setq c-basic-offset 4)
(defun use-stroustrup ()  (c-set-style "stroustrup"))
(add-to-hook-list 'c-mode-hook 'use-stroustrup)

;; Auto Modes

(setq auto-mode-alist 
      (append '(
		("\\.pl$" . prolog-mode)
		("[Mm]akefile" . shell-script-mode)
		)
	      auto-mode-alist
	      '(
		("\\(^\\|/\\)\\.[^/]+$" . shell-script-mode))))

(setq aquamacs-default-major-mode 'text-mode)

;; Autoloads

(autoload 'set-buffer-file-eol-type "set-buffer-file-eol-type" "explicitly set the text file type" t)

;; Start emacsclient server

(server-start)



