;###################################################################
;#
;# Misc. Functions
;#
;###################################################################

(provide 'kautz-misc)

(defun wrap ()
  "Wrap long lines from point to end of buffer"
  (interactive)
  (save-excursion
    (let (done linestart (max 80) (pretty t))
      (re-search-forward "^$")
      (while (not done)
	(end-of-line)
	(while (> (current-column) max)
	  (beginning-of-line)
	  (setq linestart (point))
	  (forward-char max)
	  (if pretty
	      (if (re-search-backward "[ \t]" linestart t)
		  (forward-char 1)))
	  (insert "\n")
	  (end-of-line))
	(setq done (= (point) (progn (forward-line 1) (point))))
	)
      )
    )
  )



(defconst capitalize-title-stop-words
   (concat
      "the\\|and\\|of\\|is\\|a[^.]\\|an\\|of\\|for\\|in\\|to\\|in\\|on\\|at\\|"
      "by\\|with\\|that\\|its")
   "Words not to be capitialized in a title (unless they are the first
word in the title)")

(defconst capitalize-title-stop-regexp
   (concat "\\(" capitalize-title-stop-words "\\)\\(\\b\\|'\\)"))

(defun capitalize-title-region (begin end)
   "Like capitalize-region, but don't capitalize stop words, except the first"
   (interactive "r")
   (let ((case-fold-search nil) (orig-syntax-table (syntax-table)))
      (unwind-protect
	 (save-restriction
	    (set-syntax-table text-mode-syntax-table)
	    (narrow-to-region begin end)
	    (goto-char (point-min))
	    (if (looking-at "[A-Z][a-z]*[A-Z]")
	       (forward-word 1)
	       (capitalize-word 1))
	    (while (re-search-forward "\\<" nil t)
	       (if (looking-at "[A-Z][a-z]*[A-Z]")
		  (forward-word 1)
		  (if (let ((case-fold-search t))
			 (looking-at capitalize-title-stop-regexp))
		     (downcase-word 1)
		     (capitalize-word 1)))
	       ))
	 (set-syntax-table orig-syntax-table))))


(defun capitalize-title (s)
  "Like capitalize, but don't capitalize stop words, except the first"
  (with-current-buffer (get-buffer-create "$$$Scratch$$$")
    (set-buffer (get-buffer-create "$$$Scratch$$$"))
    (erase-buffer)
    (insert s)
    (capitalize-title-region (point-min) (point-max))
    (buffer-string)))



(defun find-doubles ()
  "Find the next doubled word"
  (interactive)
  (re-search-forward "\\b\\(\\w+\\)[ \t\n]+\\1\\b"))


(defun transpose-previous ()
   "transpose the previous two characters"
   (interactive)
   (transpose-chars -1)
   (forward-char 1))

(defun copy-from-other-window ()
   "copy one line at a time from the other window"
   (interactive)
   (other-window -1)
   (copy-region-as-kill (point) (progn (forward-line 1) (point)))
   (other-window 1)
   (yank)
   (backward-delete-char 1 nil)
   (call-interactively (key-binding "\^M"))
   (setq this-command 'other-window)
   )

(defun copy-line-as-kill ()
   "copy current line to kill buffer; if at end of line, then move
to start of next line"
   (interactive)
   (if (eq last-command 'copy-line-as-kill) (append-next-kill))
   (copy-region-as-kill (point) (progn
				   (if (eolp)
				      (forward-line 1)
				      (end-of-line))
				   (point)))
				   )

(defun copy-sentence-as-kill ()
   "copy current sentence to kill buffer, and move down"
   (interactive)
   (if (eq last-command 'copy-sentence-as-kill) (append-next-kill))
   (copy-region-as-kill (point) (progn
				   (forward-sentence)
				   (point)
				   )))

(defun copy-sexp-as-kill ()
   "copy current sexp to kill buffer, and move forward"
   (interactive)
   (if (eq last-command 'copy-sexpr-as-kill) (append-next-kill))
   (copy-region-as-kill (point) (progn
				   (forward-sexp)
				   (point)
				   )))

(defun format-for-excel ()
  "replace whitespace sequences that contain tab by one tab"
   (interactive)
   (save-excursion
     (goto-char (point-min))
     (replace-regexp " *\t[ \t]*" "\t")
     ))


(defun zap-underscores ()
   "remove underlining characters from current buffer, works on
write-protected buffers"
   (interactive)
   (save-excursion
     (let (buffer-read-only)
       (beginning-of-buffer)
       (replace-string "_\^H" "" nil)
       )))

(defun bye ()
  "Save all modified files and then cleanup quietly"
  (interactive)
  (save-some-buffers t)
  (cleanup))

(defun cleanup () 
  "Kill all buffers associated with files, erase shell buffers"
  (interactive)
  (switch-to-buffer "*scratch*")
  (delete-other-windows (selected-window))
  (mapcar (function (lambda (b)
		      (if (buffer-live-p b)
			  (progn
			    (set-buffer b)
			    (if (or
				 (buffer-file-name)
				 (memq major-mode '(rmail-summary-mode
						    dired-mode
						    mail-mode
						    Buffer-menu-mode
						    un*x-apropos-mode))
				 (string-match "\\*Manual" (buffer-name)))
				(kill-buffer b)
			      (let ((buffer-read-only))
				(if (or
				     (memq major-mode '(shell-mode 
							lisp-mode
							lisp-interaction-mode
							)))
				    (erase-buffer))))))))
	  (buffer-list))
  (switch-to-buffer "*scratch*")
  )


(defun comment-out-region (comment start end)
   "Insert comment string at start of each line in region"
   (interactive "sComments begin with: \nr")
   (save-excursion
      (save-restriction
	 (narrow-to-region start end)
	 (goto-char (point-min))
	 (replace-regexp "^" comment))))

(defun find-nonprinting-chars ()
   "i-search for nonprinting characters"
   (interactive)
   (re-search-forward "[^\n\t -~]"))

(defun zap-buffer-nonprinting-chars ()
   "replace non-printing chars in entire buffer"
   (interactive)
   (zap-region-nonprinting-chars (point-min) (point-max)))

(defun zap-region-nonprinting-chars (start end )
   "replace non-printing characters between START and END"
   (let ((orig-start (point-min)) (orig-end (point-max)))
      (save-excursion
	 (save-restriction
	    (narrow-to-region start end)
	    (goto-char (point-min))
	    (replace-string "–" "-")
	    (goto-char (point-min))
	    (replace-string "‘" "'")
	    (goto-char (point-min))
	    (replace-string "’" "'")
	    (goto-char (point-min))
	    (replace-string "“" "\"")
	    (goto-char (point-min))
	    (replace-string "”" "\"")
	    (goto-char (point-min))
	    (replace-string "\u0085" "...")
	    (goto-char (point-min))
	    (replace-string "\u0096" "-")
	    (goto-char (point-min))
	    (replace-string "\u0099" "(TM)")
	    (goto-char (point-min))
	    (replace-string "\u00A9" "(c)")
    	    (goto-char (point-min))
	    (replace-string "\xf0b7" "*")
    	    (goto-char (point-min))
	    (replace-string "\u00A0" " ")
	    (goto-char (point-min))
	    (replace-regexp "[^\n\t -~]" "???")
	    ))))


(defun insert-file-name-with-completion (name)
  "Prompts for a file or directory name and inserts that name after point.
 Includes complete path prefix.
 The name may be non-existent.  Useful in Shell mode."
  (interactive "FInsert file name: ")
  (insert  name))

(defun insert-file-name-tail (name) 
 "Prompts for a file or directory name and inserts that name after point.
 Does not include unnecessary directory path prefixes.
 The name may be non-existent.  Useful in Shell mode."  
   (interactive "FInsert file name tail: ")
   (if (string-match (concat "^" (expand-file-name default-directory))
	  (expand-file-name name))
      (setq name (substring (expand-file-name name) (match-end 0))))
   (insert name))



(defun indent-to-cursor (p min max) 
 "indent region to the column of point,
 rigidly unless a prefix arg is given"
   (interactive "p\nr")
   (let ((i (current-column))
	   (w (current-indentation))
	   (origp (point)))
      (goto-char min)
      (beginning-of-line nil)
      (setq min (point))
      (goto-char max)
      (end-of-line nil)
      (setq max (point))
      (goto-char origp)
      (if (= p 1)
	 (progn
	    (indent-rigidly min max (- i w)))
	 (indent-region min max i))
      )
   )

(defun trim-line ()
   "delete trailing blanks and tabs from current line"
   (interactive)
   (save-excursion
      (end-of-line)
      (while (and (not (bolp))
		  (or (equal (char-after (1- (point))) ? )
		      (equal (char-after (1- (point))) ?\t)))
	     (backward-delete-char 1))))

(defun trim-buffer ()
   "delete all trailing whitespace"
   (interactive)
   (save-excursion
      (goto-char (point-min))
      (trim-line)
      (end-of-line)
      (while (not (eobp))
	 (forward-line 1)
	 (trim-line)
	 (end-of-line))))


(defun calculate-sum-region (begin end)
  "Sum up all numbers in region BEGIN to END and insert result at point.
Ignore all commas.  Numbers are separated by any non-numeric characters
except comma, and optionally begin with a -.  Only works for integers."
  (interactive "r")
  (let ((total 0) (text (buffer-substring begin end)))
    (while (string-match "," text)
      (setq text (concat (substring text 0 (match-beginning 0))
			 (substring text (match-end 0)))))
    (while (string-match "-?[0-9]+" text)
      (setq total (+ total (car (read-from-string 
				 (substring text (match-beginning 0)
					    (match-end 0))))))
      (setq text (substring text (match-end 0))))
    (princ (pretty-number total) (current-buffer))))

(defun calculate-sum-rectangle (begin end)
  "Sum up all numbers in rectangle whose opposite corners
are BEGIN to END and insert result into the kill ring.
Ignore all commas.  Numbers are separated by any non-numeric characters
except comma, and optionally begin with a -.  Only works for integers."
  (interactive "r")
  (copy-rectangle-to-register ?Z begin end nil)
  (set-buffer (get-buffer-create "~~junk~~"))
  (erase-buffer)
  (insert-register ?Z)
  (goto-char (point-max))
  (insert " ")
  (let ((answ-start (point)) answ)
    (calculate-sum-region (point-min) (point-max))
    (setq answ (buffer-substring answ-start (point-max)))
    (copy-region-as-kill answ-start (point-max))
    (kill-buffer (current-buffer))
    (message "Sum is %s   C-y (yank) to insert" answ))
  )
  

(defun pretty-number (n)
  "Return string of N with commas inserted appropriately"
  (let ((s (format "%d" n)))
    (while (string-match "[0-9][0-9][0-9][0-9]\\($\\|,\\)" s)
      (setq s (concat 
	       (substring s 0 (+ 1 (match-beginning 0)))
	       ","
	       (substring s (+ 1 (match-beginning 0))))))
    s))

(defun swap-windows ()
   "Reverse contents of current window and next window"
   (interactive)
   (let (window-list first-window-buffer current-buffer next-buffer)
      (setq current-buffer (current-buffer))
      (setq next-buffer (window-buffer (next-window (selected-window))))
      (switch-to-buffer next-buffer)
      (other-window 1)
      (switch-to-buffer current-buffer)))

(defun squeeze-excess-blank-lines (all)
  "Replace multiple blank lines by a single blank line in the current
buffer (or restriction), and delete trailing blank lines.  With
prefix argument delete ALL blank lines."
  (interactive "P")
  (save-excursion
    (goto-char (point-min))
    (while (and (not (eobp)) (re-search-forward "^[ \t]*$" nil t))
	   (if all (delete-region (point)
				  (progn (forward-line 1)
					 (point)))
	       (forward-line 1))
	   (while (and (not (eobp)) (looking-at "^[ \t]*$"))
		  (delete-region (point) (progn (forward-line 1)
						(point)))))
    (goto-char (point-max))
    (beginning-of-line)
    (if (looking-at "^[ \t]+$")
	(delete-region (point) (progn (end-of-line) (point)))
	(if (eobp)
	    (progn (forward-line -1)
		   (if (looking-at "^[ \t]*$")
		       (delete-region (point)
				      (progn
					(forward-line 1)
					(point)))))))))

;;; Note: the following function what-cursor-and-line-position
;;; incorporates code from the file simple.el distributed with GNU
;;; Emacs.  The GNU Emacs general licensing conditions apply to the
;;; following function
(defun what-cursor-and-line-position ()
  "Print info on cursor position (on screen and within buffer)."
  (interactive)
  (let* ((char (following-char))
	 (beg (point-min))
	 (end (point-max))
         (pos (point))
	 (total (buffer-size))
	 (percent (if (> total 50000)
		      ;; Avoid overflow from multiplying by 100!
		      (/ (+ (/ total 200) (1- pos)) (max (/ total 100) 1))
		    (/ (+ (/ total 2) (* 100 (1- pos))) (max total 1))))
	 (hscroll (if (= (window-hscroll) 0)
		      ""
		    (format " Hscroll=%d" (window-hscroll))))
	 (line (save-restriction
		 (widen)
		 (save-excursion
		   (beginning-of-line)
		   (1+ (count-lines 1 (point))))))
	 (col (current-column)))

    (if (= pos end)
	(if (or (/= beg 1) (/= end (1+ total)))
	    (message "line=%d point=%d of %d(%d%%) <%d - %d>  column %d %s"
		     line pos total percent beg end col hscroll)
	  (message "line=%d point=%d of %d(%d%%)  column %d %s"
		   line pos total percent col hscroll))
      (if (or (/= beg 1) (/= end (1+ total)))
	  (message "Char: %s (0%o)  line=%d point=%d of %d(%d%%) <%d - %d>  column %d %s"
		   (single-key-description char) char line pos total percent beg end col hscroll)
	(message "Char: %s (0%o)  line=%d point=%d of %d(%d%%)  column %d %s"
		 (single-key-description char) char line pos total percent col hscroll)))))

(defun word-count ()
  "Like the unix wc command"
  (interactive)
  (let ((lines (count-lines (point-min) (point-max)))
	(chars (- (point-max) (point-min)))
	(words (save-excursion
		 (goto-char (point-min))
		 (count-matches-value "\\w\\b"))))
    (message "%d lines, %d words, %d chars in buffer"
	     lines words chars)))
	       
(defun count-matches-value (regexp)
  "Return the number of matches for regexp past point.  Do
not print anything (unlike count-matches)."
  (let ((n 0))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward regexp nil t)
	     (setq n (1+ n)))
      n)))

(defun file-name-under-point ()
  "Return file name (any string separated by blanks, tabs, or 
newlines) under point in current buffer"   
  (save-excursion   
    (let (beg end)
      (if (looking-at "[ \t\n]")
	  (re-search-backward "[^ \t\n]"))
      (re-search-backward "^\\| \\|\t")
      (if (looking-at " \\|\t") (forward-char 1))
      (setq beg (point))
      (re-search-forward "$\\| \\|\t")
      (backward-char 1)
      (if (not (looking-at " \\|\t"))
	(forward-char 1))
      (setq end (point))
      (buffer-substring beg end))))
			      

(defun visit-file-pointer (filename)
  "Visit file whose name is under point in the current buffer"
  (interactive (list (read-string 
		      (concat "Visit file ("
			      (file-name-under-point)
			      "): "))))
  (if (string= "" filename) (setq filename (file-name-under-point)))
  (find-file filename))

(defun number-lines (beg end)
  "Number the lines in the region"
  (interactive "r")
  (goto-char end)
  (let ((count 1) (done (set-marker (make-marker) (point))))
    (goto-char beg)
    (while (< (point) (marker-position done))
      (insert (int-to-string count))
      (insert ".  ")
      (forward-line 1)
      (setq count (+ 1 count)))
    (set-marker done nil ))
  )

(defun raw-tab ()
  (interactive)
  (insert "\t")
  )

(defun use-raw-tab ()
  (local-set-key "\t" 'raw-tab)
  )

(defun tab-toggle ()
  "Toggle tab mode in current buffer"
  (interactive)
  (if (equal (key-binding "\t") 'raw-tab)
      (local-set-key "\t" 'indent-relative)
    (local-set-key "\t" 'raw-tab)
    )
  )

