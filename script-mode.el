(provide 'script-mode)

(defvar script-mode-map (copy-keymap text-mode-map))
(defvar script-mode-syntax-table (copy-syntax-table text-mode-syntax-table))

(defun insert-and-blink-matching-char (c)
  "Insert char C (interactively, last command char) and blink previous 
C, without scrolling screen"
  (interactive (list last-input-event))
  (let ((p (point)) (s (window-start)))
    (insert c)
    (goto-char p)
    (if (search-backward (char-to-string c) nil t)
	(if (>= (point) s)
	    (sit-for 1)
	    (message (concat "Matches: "
			     (buffer-substring (point)
					       (progn (end-of-line)
						      (point)))))))
    (goto-char (1+ p))))


(modify-syntax-entry ?\" "." script-mode-syntax-table)
(modify-syntax-entry ?' "." script-mode-syntax-table)
(modify-syntax-entry ?` "." script-mode-syntax-table)

(define-key script-mode-map "'" 'insert-and-blink-matching-char)
(define-key script-mode-map "`" 'insert-and-blink-matching-char)
(define-key script-mode-map "\"" 'insert-and-blink-matching-char)

(defun script-mode ()
  "Mode for editing shell scripts"
  (interactive)
  (kill-all-local-variables)
  (use-local-map script-mode-map)
  (set-syntax-table script-mode-syntax-table)
  (setq major-mode 'script-mode)
  (setq mode-name "Script")
  (auto-fill-mode -1))
